import { Service } from "typedi";
import nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";
import { getTestMailAccount } from "../../utils/test-utils/getTestMailAccount";

@Service()
export class MailerService {
  private transporter: Mail | null = null;

  async sendEmail(email: string, mailOptions: Mail.Options): Promise<void> {
    const testAccount = await getTestMailAccount();
    if (!this.transporter) {
      this.transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: testAccount.user, // generated ethereal user
          pass: testAccount.pass // generated ethereal password
        }
      });
    }
    const info = await this.transporter.sendMail({
      from: '"Magaz Team", ',
      to: email,
      ...mailOptions
    });
    console.log(nodemailer.getTestMessageUrl(info));
  }
}
