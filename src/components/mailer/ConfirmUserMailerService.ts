import { Service } from "typedi";

import { MailerService } from "./MailerService";

@Service()
export class ConfirmUserMailerService {
  constructor(private readonly mailerService: MailerService) {}

  public send(email: string, confirmCode: string): void {
    this.mailerService
      .sendEmail(email, {
        subject: "Email confirmation",
        text: this.generateConfirmLink(confirmCode)
      })
      .then(() => {
        return;
      });
  }

  private generateConfirmLink(confirmCode: string): string {
    return `http://localhost:3000/user/confirm?code=${confirmCode}`;
  }
}
