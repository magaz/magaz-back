import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository, FindOneOptions, UpdateResult, DeepPartial } from "typeorm";
import { Service } from "typedi";

import { QueryDeepPartialEntity } from "typeorm/query-builder/QueryPartialEntity";
import { User } from "../../models/User";
import { RoleEnum } from "../../utils/enums/Role.enum";
import { BaseDAL } from "../../utils/types/BaseDAL";

@Service()
export class UserDAL implements BaseDAL<User> {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) {}

  async findByIds(ids: string[]): Promise<User[]> {
    return await this.userRepository.findByIds(ids, { relations: ["roles"] });
  }

  async findOneById(id: string): Promise<User | undefined> {
    return await this.userRepository.findOne(id, { relations: ["roles"] });
  }

  async findOne(options: FindOneOptions<User>): Promise<User | undefined> {
    return await this.userRepository.findOne(options);
  }

  async createOne(user: DeepPartial<User>): Promise<User> {
    const userData = await this.userRepository.create({
      ...user,
      roles: [{ roleId: RoleEnum.user }]
    });

    return await this.userRepository.save(userData);
  }

  async find(): Promise<User[]> {
    return this.userRepository.find();
  }

  async update(
    userId: string,
    data: QueryDeepPartialEntity<User>
  ): Promise<UpdateResult> {
    return this.userRepository.update(userId, data);
  }
}
