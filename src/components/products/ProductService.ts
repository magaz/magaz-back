import { Service } from "typedi";
import { ProductDAL } from "./ProductDAL";
import { Product } from "../../models/Product";
import { CreateProductInputType } from "./ProductInputTypes";

@Service()
export class ProductService {
  constructor(private readonly productDAL: ProductDAL) {}

  async createProduct(productInput: CreateProductInputType): Promise<Product> {
    return await this.productDAL.createOne(productInput);
  }
}
