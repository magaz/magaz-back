import { InputType, Field } from "type-graphql";
import { Product } from "../../models/Product";

@InputType()
export class CreateProductInputType implements Partial<Product> {
  @Field()
  name!: string;

  @Field()
  description!: string;

  @Field()
  inStock!: number;

  @Field()
  price!: number;
}
