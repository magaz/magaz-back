import { Ctx, Query, Resolver } from "type-graphql";

import { UserDAL } from "../users/UserDAL";
import { RefreshTokenDAL } from "./RefreshTokenDAL";
import { AuthenticationResponse, Context } from "../../utils/types/gql-types";
import { RefreshTokenService } from "./RefreshTokenService";

@Resolver()
export class RefreshTokenResolver {
  constructor(
    private readonly userDAL: UserDAL,
    private readonly refreshTokenDAL: RefreshTokenDAL,
    private readonly refreshTokenService: RefreshTokenService
  ) {}

  @Query(returns => AuthenticationResponse)
  async refreshToken(@Ctx() context: Context): Promise<AuthenticationResponse> {
    return this.refreshTokenService.refreshToken(context.cookies.refresh_token);
  }
}
