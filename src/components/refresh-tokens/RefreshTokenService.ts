import { Service } from "typedi";
import { UserDAL } from "../users/UserDAL";
import { RefreshTokenDAL } from "./RefreshTokenDAL";
import { AuthenticationResponse } from "../../utils/types/gql-types";
import { AuthenticationError } from "apollo-server";
import { ErrorMessages } from "../../utils/enums/ErrorMessages.enum";
import { generateAccessToken } from "../../utils/helpers/accessToken";

@Service()
export class RefreshTokenService {
  constructor(
    private readonly userDAL: UserDAL,
    private readonly refreshTokenDAL: RefreshTokenDAL
  ) {}

  async refreshToken(refreshToken: string): Promise<AuthenticationResponse> {
    if (!refreshToken)
      throw new AuthenticationError(ErrorMessages.INVALID_REFRESH_TOKEN);

    const dbToken = await this.refreshTokenDAL.findOneById(refreshToken);

    if (!dbToken)
      throw new AuthenticationError(ErrorMessages.INVALID_REFRESH_TOKEN);

    const user = await this.userDAL.findOne({ where: { id: dbToken.user } });

    if (!user)
      throw new AuthenticationError(ErrorMessages.INVALID_REFRESH_TOKEN);

    return { token: await generateAccessToken(user.id), user };
  }
}
