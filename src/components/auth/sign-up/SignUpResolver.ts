import { Arg, Ctx, Mutation, Resolver } from "type-graphql";

import { AuthenticationResponse, Context } from "../../../utils/types/gql-types";
import { SignUpInputType } from "../shared/AuthInputTypes";
import { SignUpService } from "./SignUpService";

@Resolver()
export class SignUpResolver {
  constructor(private readonly signUpService: SignUpService) {}

  @Mutation(returns => AuthenticationResponse)
  async signUp(
    @Arg("user", type => SignUpInputType) signUpInput: SignUpInputType,
    @Ctx() context: Context
  ): Promise<AuthenticationResponse> {
    return this.signUpService.signUp(signUpInput, context);
  }
}
