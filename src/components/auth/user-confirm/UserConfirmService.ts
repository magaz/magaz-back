import { Service } from "typedi";

import { UserDAL } from "../../users/UserDAL";
import { RefreshTokenDAL } from "../../refresh-tokens/RefreshTokenDAL";
import { ConfirmCodeService } from "../shared/ConfirmCodeService";
import { Context } from "../../../utils/types/gql-types";

@Service()
export class UserConfirmService {
  constructor(
    private readonly userDAL: UserDAL,
    private readonly refreshTokenDAL: RefreshTokenDAL,
    private readonly confirmCodeService: ConfirmCodeService
  ) {}

  async confirmUser(code: string): Promise<boolean> {
    const userId = await this.confirmCodeService.getUserByCode(code);

    if (!userId) throw new Error("Code not found");

    await this.userDAL.update(userId, { confirmed: true });

    this.confirmCodeService.deleteCode(code);

    return true;
  }

  async resendConfirmCode(userId: string, context: Context): Promise<boolean> {
    const { user } = context;

    if (userId !== user?.id) throw new Error("Atata");

    const isRecharge = await this.confirmCodeService.canResendCode(user.email);

    if (isRecharge) throw new Error("Try again later");

    this.confirmCodeService.generateAndSendConfirmCode(user);

    return true;
  }
}
