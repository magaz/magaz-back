import { Service } from "typedi";
import { RedisClient } from "../../redis/RedisClient";
import { Constants } from "../../../utils/enums/constants";
import { v4 } from "uuid";
import { User } from "../../../models/User";
import { ConfirmUserMailerService } from "../../mailer/ConfirmUserMailerService";

@Service()
export class ConfirmCodeService {
  private static RECHARGE_POSTFIX = "_confirm_recharge";

  constructor(
    private readonly redisClient: RedisClient,
    private readonly confirmUserMailService: ConfirmUserMailerService
  ) {}

  async generateAndSendConfirmCode(user: User): Promise<void> {
    const confirmCode = v4();

    this.setLinkExpirationTime(confirmCode, user.id);
    this.setRechargeTime(user.email);
    this.confirmUserMailService.send(user.email, confirmCode);
  }

  async deleteCode(confirmCode: string): Promise<void> {
    this.redisClient.del(confirmCode);
  }

  getUserByCode(confirmCode: string): Promise<string | null> {
    return this.redisClient.get(confirmCode);
  }

  async canResendCode(email: string): Promise<boolean> {
    return (await this.getRechargeTime(email)) !== "true";
  }

  private setLinkExpirationTime(confirmCode: string, userId: string): void {
    this.redisClient.setExp(
      confirmCode,
      Constants.CONFIRM_LINK_EXPIRATION_TIME,
      userId
    );
  }

  private setRechargeTime(email: string): void {
    this.redisClient.setExp(
      `${email}${ConfirmCodeService.RECHARGE_POSTFIX}`,
      Constants.CONFIRM_RECHARGE_TIME,
      "true"
    );
  }

  private getRechargeTime(email: string): Promise<string | null> {
    return this.redisClient.get(
      `${email}${ConfirmCodeService.RECHARGE_POSTFIX}`
    );
  }
}
