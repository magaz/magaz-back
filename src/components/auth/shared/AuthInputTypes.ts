import { InputType, Field } from "type-graphql";
import {
  MaxLength,
  MinLength,
  IsEmail,
  Length,
  IsDate,
  IsNotEmpty
} from "class-validator";
import { User } from "../../../models/User";
import { ValidationMessages } from "../../../utils/enums/ValidationMessages.enum";

@InputType()
export class LoginInputType implements Partial<User> {
  @Field()
  @IsNotEmpty()
  email!: string;

  @Field()
  @IsNotEmpty()
  password!: string;
}

@InputType()
export class SignUpInputType implements Partial<User> {
  @Field()
  @MaxLength(30, {
    message: ValidationMessages.name_max_length
  })
  name!: string;

  @Field()
  @IsEmail()
  @MaxLength(50)
  email!: string;

  @Field()
  @MaxLength(20)
  @MinLength(3)
  password!: string;

  @Field()
  @Length(11)
  phone!: string;

  @Field()
  @IsDate()
  dateOfBirth!: Date;
}
