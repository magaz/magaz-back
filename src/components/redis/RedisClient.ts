import { Service } from "typedi";
import IORedis from "ioredis";

import { env } from "../../config/env";

@Service()
export class RedisClient {
  private readonly client: IORedis.Redis;

  constructor() {
    const options = env.redis;
    this.client = new IORedis(options.port, options.host, {
      password: options.password
    });
  }

  setExp(
    key: string,
    seconds: number,
    value: string
  ): void | Promise<IORedis.Ok> {
    return this.client.setex(key, seconds, value);
  }

  get(key: string): Promise<string | null> {
    return this.client.get(key);
  }

  del(key: string): void | Promise<number> {
    return this.client.del(key);
  }

  disconnect(): void {
    this.client.disconnect();
  }
}
