/*
 * Order resolver
 */
import {
  Resolver,
  Query,
  Authorized,
  Arg,
  Mutation,
  Ctx,
  Root,
  FieldResolver
} from "type-graphql";
import { Order } from "../../models/Order";
import { Context } from "../../utils/types/gql-types";
import { CreateOrderInputType } from "./OrderInputTypes";
import { OrderDAL } from "./OrderDAL";
import { RoleEnum } from "../../utils/enums/Role.enum";

@Resolver(of => Order)
export class OrderResolver {
  constructor(private readonly orderDAL: OrderDAL) {}

  @Query(returns => [Order], { nullable: true })
  @Authorized(RoleEnum.user)
  async orders(@Ctx() context: Context): Promise<Order[]> {
    return this.orderDAL.find({
      relations: ["itemSets", "itemSets.items", "itemSets.items.product"],
      loadRelationIds: { relations: ["user"] }
    });
  }

  @Mutation(returns => Order)
  @Authorized()
  async createOrder(
    @Ctx() context: Context,
    @Arg("order") orderInput: CreateOrderInputType
  ): Promise<Order> {
    return this.orderDAL.createOne({
      ...orderInput,
      user: context.user
    });
  }

  @FieldResolver()
  async user(@Root() order: Order, @Ctx() context: Context) {
    if (order.user && typeof order.user !== "string") {
      return order.user;
    }
    return await context.loaders.userLoader.load(
      (order.user as unknown) as string
    );
  }
}
