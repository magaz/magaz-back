import { Resolver, FieldResolver, Root, Ctx } from "type-graphql";
import { OrderItem } from "../../models/OrderItem";
import { Context } from "../../utils/types/gql-types";

@Resolver(of => OrderItem)
export class OrderItemResolver {
  @FieldResolver()
  async product(@Root() orderItem: OrderItem, @Ctx() context: Context) {
    if (typeof orderItem.product !== "string") {
      return orderItem.product;
    }

    return await context.loaders.productLoader.load(orderItem.product);
  }
}
