import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository, FindManyOptions, FindOneOptions } from "typeorm";

import { Order } from "../../models/Order";
import { BaseDAL } from "../../utils/types/BaseDAL";

@Service()
export class OrderDAL implements BaseDAL<Order> {
  constructor(
    @InjectRepository(Order) private readonly orderRepository: Repository<Order>
  ) {}

  async find(options: FindManyOptions): Promise<Order[]> {
    return await this.orderRepository.find(options);
  }

  async findOneById(id: string): Promise<Order | undefined> {
    return await this.orderRepository.findOne(id);
  }

  async findOne(options: FindOneOptions): Promise<Order | undefined> {
    return await this.orderRepository.findOne(options);
  }

  async createOne(orderInput: Partial<Order>): Promise<Order> {
    const order = await this.orderRepository.create(orderInput);
    return await this.orderRepository.save(order);
  }
}
