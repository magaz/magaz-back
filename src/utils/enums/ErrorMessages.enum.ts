export enum ErrorMessages {
  "NO_USER_FOUND" = "Неверный логин/пароль",
  "USER_ALREADY_EXISTS" = "Данный e-mail уже используется",
  "INVALID_REFRESH_TOKEN" = "Invalid refresh token"
}
