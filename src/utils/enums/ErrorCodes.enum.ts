export enum ErrorCodes {
  "NO_USER_FOUND" = "NO_USER_FOUND",
  "USER_ALREADY_EXISTS" = "USER_ALREADY_EXISTS"
}
