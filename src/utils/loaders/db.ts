/*
 * * DB connection loader
 */

import Container from "typedi";
import { createConnection, useContainer, Connection } from "typeorm";

import { env } from "../../config/env";

export const loadDb = async (): Promise<Connection> => {
  useContainer(Container);

  const { db } = env;
  return await createConnection({
    ...db,
    entities: ["src/models/*{.ts,.js}"],
    logger: "advanced-console",
    cache: true
  });
};
