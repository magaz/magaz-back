import { Context } from "../types/gql-types";

const refreshTokenName = "refresh_token";

export const setRefreshToken = (token: string, context: Context): void => {
  context.req?.res?.cookie(refreshTokenName, token, {
    httpOnly: true,
    maxAge: 1000 * 60 * 60 * 24 * 31,
    secure: process.env.NODE_ENV === "production",
    sameSite: process.env.NODE_ENV === "production"
  });
};

export const clearRefreshToken = (context: Context): void => {
  context.req?.res?.clearCookie(refreshTokenName);
};
