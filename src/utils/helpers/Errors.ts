import { ApolloError } from "apollo-server";
import { ErrorMessages } from "../enums/ErrorMessages.enum";
import { ErrorCodes } from "../enums/ErrorCodes.enum";

export class NoUserError extends ApolloError {
  constructor() {
    super(ErrorMessages.NO_USER_FOUND, ErrorCodes.NO_USER_FOUND);
  }
}

export class UserExistsError extends ApolloError {
  constructor() {
    super(ErrorMessages.USER_ALREADY_EXISTS, ErrorCodes.USER_ALREADY_EXISTS);
  }
}
