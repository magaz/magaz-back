import jwt from "jsonwebtoken";
import { env } from "../../config/env";
import { Jwt } from "./authChecker";

export const generateAccessToken = async (userId: string): Promise<string> => {
  return jwt.sign({ userId: userId }, env.appKey, {
    expiresIn: 600000 //TODO: prolong
  });
};

export const verifyAccessToken = async (token: string): Promise<Jwt> => {
  return (await jwt.verify(token, env.appKey)) as Jwt;
};
