/*
 * Auth checker middleware
 */

import { AuthChecker } from "type-graphql";
import { Context } from "../types/gql-types";
import { verifyAccessToken } from "./accessToken";
import Container from "typedi";
import { UserDAL } from "../../components/users/UserDAL";

export const authChecker: AuthChecker<Context> = async (
  { root, args, context, info },
  roles
) => {
  if (!context.authorization) return false;
  const result = await verifyAccessToken(context.authorization);
  if (!result) return false;

  const user = await Container.get(UserDAL).findOneById(result.userId);

  // * no user or his roles
  if (!user || user.roles.length === 0) return false;

  // * gain access if user has one of provided roles
  if (
    roles.length !== 0 &&
    !user.roles.some(
      r => ((roles as unknown) as number[]).indexOf(r.roleId) >= 0
    )
  ) {
    return false;
  }

  context.user = user;

  return true;
};

export type Jwt = {
  userId: string;
  iat: number;
  exp: number;
};
