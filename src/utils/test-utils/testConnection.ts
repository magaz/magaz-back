import { Connection, useContainer, createConnection } from "typeorm";
import Container from "typedi";

export const testConnection = async (
  dropSchema = false
): Promise<Connection> => {
  useContainer(Container);
  console.log("Creating test db connection");

  return await createConnection({
    type: "postgres",
    entities: ["src/models/*{.ts,.js}"],
    logger: "advanced-console",
    cache: true,
    database: "postgres",
    password: "postgres-test",
    username: "postgres",
    port: 5431,
    host: "localhost",
    synchronize: true,
    dropSchema
  });
};
