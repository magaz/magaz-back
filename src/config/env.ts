import { ConnectionOptions } from "typeorm";

/*
 * Env file
 */
const defaultPort = 3000;

interface Environment {
  apollo: {
    introspection: boolean;
    playground: boolean;
    tracing: boolean;
  };
  redis: {
    port: number;
    password?: string;
    host: string;
  };
  logHTTP: boolean;
  emitSchema: boolean;
  db: ConnectionOptions;
  appKey: string;
  port: number | string;
  isProduction: boolean;
}

export const env: Environment = {
  apollo: {
    introspection: process.env.APOLLO_INTROSPECTION === "true",
    playground: process.env.APOLLO_PLAYGROUND === "true",
    tracing: process.env.APOLLO_TRACING === "true"
  },
  db: {
    database: process.env.TYPEORM_DATABASE || "develop",
    password: process.env.TYPEORM_PASSWORD || "develop",
    username: process.env.TYPEORM_USERNAME || "develop",
    port: ((process.env.TYPEORM_PORT as any) as number) || 5432,
    dropSchema: process.env.TYPEORM_DROP_SCHEMA === "true",
    synchronize: process.env.TYPEORM_SYNCHRONIZE === "true",
    logging: process.env.TYPEORM_LOGGING || ("error" as any),
    type: (process.env.TYPEORM_TYPE as any) || "postgres",
    host: process.env.TYPEORM_HOST || "localhost"
  },
  redis: {
    port: parseInt(process.env.REDIS_PORT || "6379"),
    host: process.env.REDIS_HOST || "localhost",
    password: process.env.REDIS_PASSWORD
  },
  logHTTP: process.env.LOG_HTTP === "true",
  emitSchema: process.env.EMIT_SCHEMA === "true",
  appKey: process.env.APP_KEY || "test",
  port: process.env.PORT || defaultPort,
  isProduction: process.env.NODE_ENV === "production"
};
