import Container from "typedi";
import DataLoader from "dataloader";
import { In } from "typeorm";
import { ProductDAL } from "../components/products/ProductDAL";
import { Product } from "../models/Product";

export const productLoader = (): DataLoader<string, Product> => {
  return new DataLoader(async keys => {
    const productIds = keys as string[];
    const products = await Container.get(ProductDAL).find({
      where: { id: In(productIds) }
    });
    const productsMap: Record<string, Product> = {};
    products.forEach(product => {
      productsMap[product.id] = product;
    });

    return productIds.map(key => productsMap[key]);
  });
};
