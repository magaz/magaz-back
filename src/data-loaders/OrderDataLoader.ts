import Container from "typedi";
import DataLoader from "dataloader";
import { In } from "typeorm";
import { OrderDAL } from "../components/orders/OrderDAL";
import { OrderStatusEnum } from "../utils/enums/OrderStatus.enum";
import { Order } from "../models/Order";

export const cartLoader = (): DataLoader<string, Order> => {
  return new DataLoader(async keys => {
    const cartIds = keys as string[];
    const cartItems: Order[] = await Container.get(OrderDAL).find({
      where: { user: In(cartIds), status: OrderStatusEnum.draft },
      relations: [
        "itemSets",
        "itemSets.items",
        "itemSets.items.product",
        "user"
      ]
    });

    const cartItemMap: Record<string, Order> = {};
    cartItems.forEach(cartItem => {
      cartItemMap[cartItem.user.id] = cartItem;
    });

    return cartIds.map(key => cartItemMap[key]);
  });
};