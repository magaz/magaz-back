/*
 * Role entity
 */

import { Field, ObjectType } from "type-graphql";
import {Entity, PrimaryColumn, ManyToMany} from "typeorm";
import { User } from "./User";
import { RoleEnum } from "../utils/enums/Role.enum";

@Entity()
@ObjectType()
export class Role {
  @PrimaryColumn()
  @Field({ defaultValue: RoleEnum.user })
  roleId!: RoleEnum;

  @ManyToMany(
    type => User,
    user => user.roles,
    { onDelete: "CASCADE" }
  )
  user!: User;
}
