/*
 * User entity
 */

import { Field, ID, ObjectType } from "type-graphql";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  Unique,
  ManyToMany,
  JoinTable
} from "typeorm";
import { Role } from "./Role";
import { RefreshToken } from "./RefreshToken";
import { Order } from "./Order";

@Entity()
@ObjectType()
@Unique(["email"])
@Unique(["phone"])
export class User {
  @Field(type => ID)
  @PrimaryGeneratedColumn("uuid")
  readonly id!: string;

  @Field()
  @Column()
  name!: string;

  @Field()
  @Column()
  email!: string;

  @Column()
  password!: string;

  @Field()
  @Column()
  phone!: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  dateOfBirth?: Date;

  @Column({ default: false })
  @Field()
  confirmed!: boolean;

  @Field(type => [Role])
  @ManyToMany(
    type => Role,
    role => role.user,
    { cascade: ["insert", "remove"] }
  )
  @JoinTable()
  roles!: Role[];

  @OneToMany(
    type => RefreshToken,
    token => token.user
  )
  tokens?: RefreshToken[];

  @OneToMany(
    type => Order,
    order => order.user,
    { cascade: ["insert"] }
  )
  orders?: Order[];

  @Field(type => Order, { nullable: true })
  cart?: Order;

  @Field()
  @CreateDateColumn()
  createdAt!: Date;

  @Field()
  @UpdateDateColumn()
  updatedAt!: Date;
}
