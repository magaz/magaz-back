/*
 * OrderItem entity
 */

import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Field, ObjectType } from "type-graphql";
import { OrderSet } from "./OrderSet";
import { Product } from "./Product";

@Entity()
@ObjectType()
export class OrderItem {
  @PrimaryGeneratedColumn("uuid")
  readonly id!: string;

  @Column()
  @Field()
  quantity!: number;

  @ManyToOne(
    type => OrderSet,
    orderSet => orderSet.items,
    { cascade: ["insert"] }
  )
  orderSet!: OrderSet;

  @Field()
  @ManyToOne(
    type => Product,
    product => product.orderItems,
    { cascade: true, eager: true }
  )
  product!: Product;
}
