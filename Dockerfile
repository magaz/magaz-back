FROM node:12.13.1 as build-deps
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn
COPY . ./

CMD ["yarn", "run", "start"]