module.exports = {
  apps: [
    {
      name: "Magaz-back",
      script: "./dist/index.js",
      exec_mode: "cluster_mode",
      node_args: "-r dotenv/config",
      instances: "max",
      max_memory_restart: "300M"
    }
  ]
};
